$(document).ready(function(){ 
    var item = $(".gallery-item"); 
    var modal = $(".modal"); 
    var img = $(".modal-img"); 
    var src = ""; 
    item.on("click", function() { 
        src = $(this).attr('src'); 
        img.attr('src', src); 
        modal.fadeIn(300); 
    }) 
    modal.on("click", function() { 
        $(this).fadeOut(300); 
    }) 
})