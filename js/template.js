$(document).ready(function() {
    //Fixed Menu
    fixedMenu();
    //Mobile Menu
    mobileMenuInit();
    //Scroll down in header
    headerScrollDown();
    //Scroll up in footer
    footerScrollUp();
    //WOW animation init
    wowInit();

    moveSection($('.hero-banner-form'), $('.form-placeholder'), $('.form-container'));
    moveSection($('.hero-banner-form'), $('.form-placeholder'), $('.features-container'));

    //ONE, TWO, THREE COLUMN HeroSlider
    smallSliderInit();
    
    //Homepage HeroSlider
    homeSliderInit();

    //Homepage testimonials
    testimonialsSliderInit();

    //Homepage Services
    var brake_array = {};
    brake_array["1200"] = 4;
    brake_array["992"] = 3;
    brake_array["768"] = 2;    
    brake_array["469"] = 1; 
    rowModuleBreaker(".homepage .services",".box-services",brake_array,"first","last");

    //Homepage Features
    var brake_array = {};
    brake_array["768"] = 2;    
    brake_array["469"] = 1; 
    rowModuleBreaker(".homepage .features",".feature",brake_array,"first","last");

    //Skills animation
    if ($(".loading-skills-wrapper").length > 0) {
        skillsInit();
    }
    questionAccordion();
    hideScrollTopOnSmallScreen();
    initBrandsSlider();


});


function showMenu() {
    var scrollTop = $(window).scrollTop();
    var b = 50;
    if (scrollTop > b) {
        $("header.header, .body-container-wrapper").removeClass('scroll');
        $("header.header, .body-container-wrapper").addClass('scroll');

    } else {
        $("header.header, .body-container-wrapper").removeClass('scroll');
    }

    if ($('header.header .slicknav_menu').css('display') == 'block')
    {
       $("header.header, .body-container-wrapper").removeClass('scroll');
    }
};

$(window).scroll(function() {
    showMenu();
});

function wowInit() {
    var wow = new WOW({
        mobile: false
    });
    wow.init();
}
function questionAccordion(){
    if($("html.hs-inline-edit").length==0){
        $("section.question").each(function(i,e){
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id","accordion");
    $("section.question .panel-group .panel").each(function(i,e){
        var href=$(e).find("h4.panel-title > a").attr("href"); 
        $(e).find("h4.panel-title > a").attr("href",href+(i+1)); 

        var id=$(e).find(".panel-collapse").attr("id"); 
        $(e).find(".panel-collapse").attr("id",id+(i+1)); 
    });
}
function headerScrollDown(){
    $(".mouse_scroll").click(function() {
        $("html, body").animate({
            scrollTop: $("#main_section").position().top - 70
        }, 1000);
        return false
    });
}
function footerScrollUp(){
    $(".scroll-top").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false
    });

}
function testimonialsSliderInit(){
    $('.slick-testimonial > div > span').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 1000,
        responsive: [
        {
          breakpoint: 767,
            settings: {
                dots:true,
                arrows: false,
            }
        }]
        });
}
function mobileMenuInit(){
    $("header.header .hs-menu-flow-horizontal > ul").slicknav({
        prependTo: "header.header",
        label: "",
        allowParentLinks:true
    });
    $(".slicknav_btn").click(function(i) {
        console.log("test");
        if ($("header.header.fixed-small").length > 0) {
            $("header.header").removeClass("fixed-small")
        } else {
            $("header.header").addClass("fixed-small")
        }
    });
}
function homeSliderInit(){
    $(".main .homeslider > span").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 1500,
  fade: true,
  cssEase: 'linear',
        responsive: [
        {
          breakpoint: 767,
            settings: {
                dots:true,
                arrows: false,
        }
        }]
    });

}

function smallSliderInit(){

    $(".sm-slider-main .slide-content > span").slick({
     dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [
        {
          breakpoint: 767,
            settings: {
                dots:false,
                arrows: true,
        }
        }]
    });


}

function fixedMenu(){
    if($(".header--static").length < 1 && $(".contact-template .header").length==0) {
        $(window).scroll(function() {
            var i = $(window).scrollTop();
            if (i > 0) {
                $("header.header").addClass("fixed");
                if ($(".menu-black").length > 0) {
                    $(".menu-black .header .black").css("display", "none");
                    $(".menu-black .header .white").css("display", "block")
                }
            } else {
                if (i == 0) {
                    $("header.header").removeClass("fixed");
                    if ($(".menu-black").length > 0) {
                        $(".menu-black .header .black").css("display", "block");
                        $(".menu-black .header .white").css("display", "none")
                    }
                }
            }
        });
    }
    if($(".contact-template .header").length>0){
        $(".contact-template .header").addClass("fixed");
    }
}
function rowModuleBreaker(parent,css_selector,break_array,class_first_row,class_last_row){
    var oldCols=0;
    $(window).resize(function(){
        var cols=0;
        var lastBreak=0;
        for(var windowBreak in break_array){
            if(window.innerWidth<=parseInt(windowBreak)){
                cols=break_array[windowBreak];
                break;
            }
            lastBreak=windowBreak;
        }

        if(cols==0){
            cols=break_array[lastBreak];
        }
        if(oldCols==0||oldCols!=cols){
            $(parent).each(function(j,selector){
            
            var old=oldCols;
            oldCols=cols;
            var inc=-1;
            var row=[];
            var length=$(selector).find(css_selector).length;
            $(selector).find(css_selector).each(function(i,e){
                if(i%cols==0){
                    inc++;
                    row=[];
                }
                row.push($(e).parent().clone(true));
                $(e).parent().parent().addClass("old_remove");
                if(i%cols==cols-1){
                    var rowContainer=$("<div class='row'></div>");
                    if(inc==0){
                        rowContainer.addClass(class_first_row);
                    }
                    if(i==length-1){
                        rowContainer.addClass(class_last_row);
                    }
                    rowContainer.append(row);
                    $(e).parent().parent().before(rowContainer);
                    
                }

                if(i%cols<cols-1 && i==length-1){
                        var rowContainer=$("<div class='row'></div>");
                        if(inc==0){
                            rowContainer.addClass(class_first_row);
                        }
                        rowContainer.addClass(class_last_row);
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                }
                if($(e).parent().parent().hasClass("hs_cos_wrapper_type_custom_widget")){
                    $(e).parent().parent().remove();
                }
            });
            $(selector).find(css_selector).parent().attr("class","span"+(12/cols));
            $(".old_remove").remove();
            });
        }
    });
    $(window).resize();
}
function skillsInit(){
    var r = 0;
            var j = $(".loader1").ClassyLoader({
            speed: 30,
            diameter: 80,
            roundedLine: true,
            fontSize: "44px",
            fontFamily: "Palanquin",
            fontColor: "#ffffff",
            lineColor: "#ffffff",
            percentage: 0,
            lineWidth: 5,
            start: "top",
            remainingLineColor: "#636362",
            animate: false
        });
        var h = $(".loader2").ClassyLoader({
            speed: 30,
            diameter: 80,
            roundedLine: true,
            fontSize: "44px",
            fontFamily: "Palanquin",
            fontColor: "#ffffff",
            lineColor: "#ffffff",
            percentage: 0,
            lineWidth: 5,
            start: "top",
            remainingLineColor: "#636362",
            animate: false
        });
        var g = $(".loader3").ClassyLoader({
            speed: 30,
            diameter: 80,
            roundedLine: true,
            fontSize: "44px",
            fontFamily: "Palanquin",
            fontColor: "#ffffff",
            lineColor: "#ffffff",
            percentage: 0,
            lineWidth: 5,
            start: "top",
            remainingLineColor: "#636362",
            animate: false
        });
        var f = $(".loader4").ClassyLoader({
            speed: 30,
            diameter: 80,
            roundedLine: true,
            fontSize: "44px",
            fontFamily: "Palanquin",
            fontColor: "#ffffff",
            lineColor: "#ffffff",
            percentage: 0,
            lineWidth: 5,
            start: "top",
            remainingLineColor: "#636362",
            animate: false
        });
        $(window).scroll(function(){
                    var w = $(window).scrollTop();
        var t = $(".skills").position().top;
        var z = $(window).height();
        var u = w + z;
        var i = $(".skills .circle1").html();
        var y = $(".skills .circle2").html();
        var x = $(".skills .circle3").html();
        var v = $(".skills .circle4").html();
        if (u - 200 > t && r == 0) {
            r = 1;
            j.setPercent(i).draw();
            h.setPercent(y).draw();
            g.setPercent(x).draw();
            f.setPercent(v).draw();
        }

        });

}


function moveSection(elementToMove, firstPlaceholder, secondPalceholder) {
    $(window).resize(function() {
        if (window.innerWidth < 801 && firstPlaceholder.find(elementToMove).length == 1) {
            secondPalceholder.prepend(elementToMove);
            elementToMove.css('margin-bottom','20px');
        } else if (window.innerWidth >= 801 && secondPalceholder.find(elementToMove).length == 1) {
            firstPlaceholder.prepend(elementToMove);
            elementToMove.css('margin-bottom','0px');
        }
    });
}
function hideScrollTopOnSmallScreen() {
    $(window).resize(function () {
        if (window.innerHeight + 220 >= $("body").height()) {
            $(".footer .scroll-top").hide();
        } else {
            $(".footer .scroll-top").show();
        }
    });
    $(window).resize();
}


function initBrandsSlider(){
    if($(".brands-slider").length>0){
        $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow:5,
            slidesToScroll: 5,
            arrows: true,
            dots: true,
              responsive: [
                {
                  breakpoint: 1150,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                  }
                },
                {
                  breakpoint: 900,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                  }
                },
                {
                  breakpoint: 760,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                }
            ]
        });
    }
}