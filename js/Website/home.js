$('.slick-testimonial-quotes>div>span').on('init', function(event, slick){
         $(event.target).find(".slick-slide[data-slick-index='1']").addClass("active-slide");
});

$('.slick-testimonial-quotes>div>span').slick({
  centerPadding: '20px',
  slidesToShow: 3,
  // autoplay: true,
  arrows: true,
  responsive: [
          {
      breakpoint: 1400,
      settings: {
        autoplay: true,
        arrows: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 971,
      settings: {
        arrows: false,
        autoplay: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        autoplay: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
$('.slick-testimonial-quotes>div>span').on('beforeChange', function(event, slick, currentSlide, nextSlide){
     if(slick.slideCount-1==currentSlide&&nextSlide==0){
         $(event.target).find(".slick-slide").removeClass("active-slide");
         $(event.target).find(".slick-slide[data-slick-index='"+(currentSlide+2)+"']").addClass("active-slide");
         $(event.target).find(".slick-slide[data-slick-index='"+(nextSlide+1)+"']").addClass("active-slide");
       
     }else if(slick.slideCount-1==nextSlide&&currentSlide==0){
         $(event.target).find(".slick-slide").removeClass("active-slide");
         $(event.target).find(".slick-slide[data-slick-index='"+(currentSlide)+"']").addClass("active-slide");
         $(event.target).find(".slick-slide[data-slick-index='"+(nextSlide+1)+"']").addClass("active-slide");
     }else{
         $(event.target).find(".slick-slide").removeClass("active-slide");
         $(event.target).find(".slick-slide[data-slick-index='"+(nextSlide+1)+"']").addClass("active-slide");
         
     }
});