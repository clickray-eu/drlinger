// $(document).ready(function(){
    $('.testimonial-drlinger > span').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        autoplaySpeed: 10000,
        arrows: true,
        dots: true,
        responsive: [
            {
                breakpoint: 850,
                settings: {
                    arrows: true,
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
// })