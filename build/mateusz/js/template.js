'use strict';

// file: cutWrapper.js
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);

$(".pricing-box__container").cutWrappers('.pricing-box');
// end file: cutWrapper.js

// file: slick-inits.js
// $(document).ready(function(){
$('.testimonial-drlinger > span').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 10000,
    arrows: true,
    dots: true,
    responsive: [{
        breakpoint: 850,
        settings: {
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }]
});
// })
// end file: slick-inits.js

// file: template.js
$(document).ready(function () {
    //Fixed Menu
    fixedMenu();
    //Mobile Menu
    mobileMenuInit();
    //Scroll down in header
    headerScrollDown();
    //Scroll up in footer
    footerScrollUp();
    //WOW animation init
    wowInit();

    //ONE, TWO, THREE COLUMN HeroSlider
    smallSliderInit();

    //Homepage HeroSlider
    homeSliderInit();

    //Homepage testimonials
    testimonialsSliderInit();

    //Homepage Services
    var brake_array = {};
    brake_array["1200"] = 4;
    brake_array["992"] = 3;
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    rowModuleBreaker(".homepage .services", ".box-services", brake_array, "first", "last");

    //Homepage Features
    var brake_array = {};
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    rowModuleBreaker(".homepage .features", ".feature", brake_array, "first", "last");

    //Skills animation
    if ($(".loader1").length > 0 && $(".loader2").length > 0 && $(".loader3").length > 0 && $(".loader4").length > 0) skillsInit();
    questionAccordion();

    //test

});

function showMenu() {
    var scrollTop = $(window).scrollTop();
    var b = 50;
    if (scrollTop > b) {
        $("header.header, .body-container-wrapper").removeClass('scroll');
        $("header.header, .body-container-wrapper").addClass('scroll');
    } else {
        $("header.header, .body-container-wrapper").removeClass('scroll');
    }

    if ($('header.header .slicknav_menu').css('display') == 'block') {
        $("header.header, .body-container-wrapper").removeClass('scroll');
    }
};

$(window).scroll(function () {
    showMenu();
});

function wowInit() {
    var wow = new WOW({
        mobile: false
    });
    wow.init();
}
function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}
function headerScrollDown() {
    $(".mouse_scroll").click(function () {
        $("html, body").animate({
            scrollTop: $("#main_section").position().top - 70
        }, 1000);
        return false;
    });
}
function footerScrollUp() {
    $(".scroll-top").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}
function testimonialsSliderInit() {
    $('.slick-testimonial > div > span').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 1000,
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}
function mobileMenuInit() {
    $("header.header .hs-menu-flow-horizontal > ul").slicknav({
        prependTo: "header.header",
        label: "",
        allowParentLinks: true
    });
    $(".slicknav_btn").click(function (i) {
        console.log("test");
        if ($("header.header.fixed-small").length > 0) {
            $("header.header").removeClass("fixed-small");
        } else {
            $("header.header").addClass("fixed-small");
        }
    });
}
function homeSliderInit() {
    $(".main .homeslider > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}

function smallSliderInit() {

    $(".sm-slider-main .slide-content > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: false,
                arrows: true
            }
        }]
    });
}

function fixedMenu() {
    if ($(".header--static").length < 1 && $(".contact-template .header").length == 0) {
        $(window).scroll(function () {
            var i = $(window).scrollTop();
            if (i > 0) {
                $("header.header").addClass("fixed");
                if ($(".menu-black").length > 0) {
                    $(".menu-black .header .black").css("display", "none");
                    $(".menu-black .header .white").css("display", "block");
                }
            } else {
                if (i == 0) {
                    $("header.header").removeClass("fixed");
                    if ($(".menu-black").length > 0) {
                        $(".menu-black .header .black").css("display", "block");
                        $(".menu-black .header .white").css("display", "none");
                    }
                }
            }
        });
    }
    if ($(".contact-template .header").length > 0) {
        $(".contact-template .header").addClass("fixed");
    }
}
function rowModuleBreaker(parent, css_selector, break_array, class_first_row, class_last_row) {
    var oldCols = 0;
    $(window).resize(function () {
        var cols = 0;
        var lastBreak = 0;
        for (var windowBreak in break_array) {
            if (window.innerWidth <= parseInt(windowBreak)) {
                cols = break_array[windowBreak];
                break;
            }
            lastBreak = windowBreak;
        }

        if (cols == 0) {
            cols = break_array[lastBreak];
        }
        if (oldCols == 0 || oldCols != cols) {
            $(parent).each(function (j, selector) {

                var old = oldCols;
                oldCols = cols;
                var inc = -1;
                var row = [];
                var length = $(selector).find(css_selector).length;
                $(selector).find(css_selector).each(function (i, e) {
                    if (i % cols == 0) {
                        inc++;
                        row = [];
                    }
                    row.push($(e).parent().clone(true));
                    $(e).parent().parent().addClass("old_remove");
                    if (i % cols == cols - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        if (i == length - 1) {
                            rowContainer.addClass(class_last_row);
                        }
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }

                    if (i % cols < cols - 1 && i == length - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        rowContainer.addClass(class_last_row);
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }
                    if ($(e).parent().parent().hasClass("hs_cos_wrapper_type_custom_widget")) {
                        $(e).parent().parent().remove();
                    }
                });
                $(selector).find(css_selector).parent().attr("class", "span" + 12 / cols);
                $(".old_remove").remove();
            });
        }
    });
    $(window).resize();
}
function skillsInit() {
    var r = 0;
    var j = $(".loader1").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var h = $(".loader2").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var g = $(".loader3").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var f = $(".loader4").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    $(window).scroll(function () {
        var w = $(window).scrollTop();
        var t = $(".skills").position().top;
        var z = $(window).height();
        var u = w + z;
        var i = $(".skills .circle1").html();
        var y = $(".skills .circle2").html();
        var x = $(".skills .circle3").html();
        var v = $(".skills .circle4").html();
        if (u - 200 > t && r == 0) {
            r = 1;
            j.setPercent(i).draw();
            h.setPercent(y).draw();
            g.setPercent(x).draw();
            f.setPercent(v).draw();
        }
    });
}

// end file: template.js

// file: Modules/va-maplocation.js

// end file: Modules/va-maplocation.js

// file: Website/dental-implants-gallery.js
$('.slick-wrapper2 > span').slick({
    dots: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1280,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
    }, {
        breakpoint: 992,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
    }, {
        breakpoint: 650,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    }]
});
// end file: Website/dental-implants-gallery.js

// file: Website/gallery.js
$(document).ready(function () {
    var item = $(".gallery-item");
    var modal = $(".modal");
    var img = $(".modal-img");
    var src = "";
    item.on("click", function () {
        src = $(this).attr('src');
        img.attr('src', src);
        modal.fadeIn(300);
    });
    modal.on("click", function () {
        $(this).fadeOut(300);
    });
});
// end file: Website/gallery.js

// file: Website/home.js
$('.slick-testimonial-quotes>div>span').on('init', function (event, slick) {
    $(event.target).find(".slick-slide[data-slick-index='1']").addClass("active-slide");
});

$('.slick-testimonial-quotes>div>span').slick({
    centerPadding: '20px',
    slidesToShow: 3,
    // autoplay: true,
    arrows: true,
    responsive: [{
        breakpoint: 1400,
        settings: {
            autoplay: true,
            arrows: true,
            centerPadding: '40px',
            slidesToShow: 1
        }
    }, {
        breakpoint: 971,
        settings: {
            arrows: false,
            autoplay: true,
            centerPadding: '40px',
            slidesToShow: 1
        }
    }, {
        breakpoint: 480,
        settings: {
            arrows: false,
            autoplay: true,
            centerPadding: '40px',
            slidesToShow: 1
        }
    }]
});
$('.slick-testimonial-quotes>div>span').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    if (slick.slideCount - 1 == currentSlide && nextSlide == 0) {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 2) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 1) + "']").addClass("active-slide");
    } else if (slick.slideCount - 1 == nextSlide && currentSlide == 0) {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + currentSlide + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 1) + "']").addClass("active-slide");
    } else {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 1) + "']").addClass("active-slide");
    }
});
// end file: Website/home.js

// file: Website/resources.js

$(document).ready(function () {

    setTimeout(function () {
        $('.resource-categories__element--active').click();
    }, 10);
});

// end file: Website/resources.js

// file: Website/smille-gallery.js
// $('.slick-wrapper > span').slick({
//   slidesToShow: 4,
//   slidesToScroll: 1,
//   autoplay: true,
//   autoplaySpeed: 5000,
// });

$('.slick-wrapper > span').slick({
    dots: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1280,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
    }, {
        breakpoint: 992,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
    }, {
        breakpoint: 650,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    }]
});
$('.slick-teeth > span').slick({
    dots: true,
    infinite: true,
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1280,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
    }, {
        breakpoint: 992,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
    }, {
        breakpoint: 650,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
            arrows: false
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    }]
});

// end file: Website/smille-gallery.js
//# sourceMappingURL=template.js.map
